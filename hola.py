#!/usr/bin/env python
# -*- coding: utf-8 -*-
import os
import random
import time


def crear_matriz(N, matriz):
    for i in range(N):
        matriz.append([0] * N)
    for i in range(N):
        for j in range(N):
            matriz[i][j] = (random.randrange(2))


def llenar_matriz(N, matriz):
    for i in range(N):
        for j in range(N):
            if matriz[i][j] == 0:
                matriz[i][j] = 'o'
            if matriz[i][j] == 1:
                matriz[i][j] = '-'


def contar(N, matriz):
    contador_vivas = 0
    contador_muertas = 0
    for i in range(N):
        for j in range(N):
            if matriz[i][j] == 'o':
                contador_vivas += 1
            if matriz[i][j] == '-':
                contador_muertas += 1
    print(" Celulas vivas: ", contador_vivas, '\n', "Celulas muertas: ", contador_muertas, '\n')


def imprimir_matriz(N, matriz, texto):
    for i in range(N):
        for j in range(N):
            texto += matriz[i][j] + ' '
        texto += ' \n'
    print(texto)


def condicionesvivas(N, matriz, matriztemp):
    for i in range(N):
        for j in range(N):
            if matriztemp[i][j] == 'o':
                contador_v = 0
                for h in range(i - 1, i + 2):
                    for k in range(j - 1, j + 2):
                        if 0 <= h <= N - 1 and 0 <= k <= N - 1 and h != i and k != j:
                            if matriztemp[h][k] == 'o':
                                contador_v += 1
                if contador_v >= 4 or contador_v <= 1:
                    matriz[i][j] = '-'
                elif contador_v == 3 or contador_v == 2:
                    matriz[i][j] = 'o'


def temporal(N, matriztemp, matriz):
    for i in range(N):
        matriztemp.append([0] * N)
        for j in range(N):
            matriztemp[i][j] = matriz[i][j]


def condicionesmuertas(N, matriz, matriztemp):
    for i in range(0, N):
        for j in range(0, N):
            if matriz[i][j] == '-':
                contador_m = 0
                for h in range(i - 1, i + 2):
                    for k in range(j - 1, j + 2):
                        if 0 <= h <= N - 1 and 0 <= k <= N - 1:
                            if matriz[h][k] == 'o':
                                contador_m += 1
                if contador_m == 3:
                    matriztemp[i][j] = 'o'


# main
N = (random.randint(5, 15))
vivas = 0
muertas = 0
matriz = []
matriztemp = []
texto = ""
crear_matriz(N, matriz)
llenar_matriz(N, matriz)
while True:
    contar(N, matriz)
    temporal(N, matriztemp, matriz)
    imprimir_matriz(N, matriz, texto)
    condicionesmuertas(N, matriz, matriztemp)
    condicionesvivas(N, matriz, matriztemp)
    time.sleep(2.4)
    os.system("cls")
