#!/usr/bin/env python
# -*- coding: utf-8 -*-
import os
import random
import time
time.sleep(2.4)
def crear_matriz(N, matriz):
    for i in range(N):
        matriz.append([0]*N)
    for i in range(N):
        for j in range(N):
            matriz[i][j] = (random.randrange(2))
def llenar_matriz(N, matriz):
    for i in range(N):
        for j in range(N):
            if matriz[i][j] == 0:
                matriz[i][j] = '.'
            if matriz[i][j] == 1:
                matriz[i][j] = 'X'
            #texto += matriz[i][j]
        #texto += '\n'
    #print("celulas vivas: ", vivas, "celulas muertas: ", muertas)
    #print(texto)
def imprimir_matriz(N, matriz, texto):
    for i in range(N):
        for j in range(N):
            texto += matriz[i][j]
        texto += '\n'
    print(texto)
def condicionesvivas(N, matriz):
    contador = 0
    for i in range(N):
        for j in range(N):
            if matriz[i][j] == '.':
                for h in range(i - 1, i + 1):
                    for k in range(j + 1, j - 1):
                        if matriz[h][k] == '.':
                            contador += 1
                if contador == 3 or contador == 2:
                    matriz[i][j] = '.'
                if contador >= 4 or contador <= 1:
                    matriz[i][j] = 'X'
def condicionesmuertas(N, matriz):
    contador = 0
    for i in range(N):
        for j in range(N):
            if matriz[i][j] == 'X':
                for h in range(i - 1, i + 1):
                    for k in range(j + 1, j - 1):
                        if matriz[h][k] == '.':
                            contador += 1
                if contador == 3:
                    matriz[i][j] = '.'
#main
N = (random.randint(5, 15))
vivas = 0
muertas = 0
matriz = []
texto = ""
crear_matriz(N, matriz)
llenar_matriz(N, matriz)
while True:
    imprimir_matriz(N, matriz, texto)
    condicionesmuertas(N, matriz)
    condicionesvivas(N, matriz)
    os.system("cls")