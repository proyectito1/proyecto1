#!/usr/bin/env python
#-*- coding: utf-8 -*-

import os
import random
import time

def crear_matriz(N, matriz):
    for i in range(N):
        matriz.append([0]*N)
    for i in range(N):
        for j in range(N):
            matriz[i][j] = (random.randrange(2))

def llenar_matriz(N, matriz):
    for i in range(N):
        for j in range(N):
            if matriz[i][j] == 0:
                matriz[i][j] = 'x'
            if matriz[i][j] == 1:
                matriz[i][j] = '.'

def contar(N, matriztemp):
    contador_vivas = 0
    contador_muertas = 0
    for i in range(N):
        for j in range(N):
            if matriztemp[i][j] == 'x':
                contador_vivas += 1
            if matriztemp[i][j] == '.':
                contador_muertas += 1
    print(" Celulas vivas: ", contador_vivas, '\n', "Celulas muertas: ", contador_muertas, '\n')

def imprimir_matriz(N, matriztemp, texto):
    for i in range(N):
        for j in range(N):
            texto += matriztemp[i][j] + ' '
        texto += ' \n'
    print(texto)

def condicionesvivas(N, matriz, matriztemp):
    contador = 0
    for i in range(N):
        for j in range(N):
            if matriz[i][j] == 'x':
                for h in range(i - 1, i + 2):
                    if 0 <= h < N:
                        for k in range(j - 1, j + 2):
                            if 0 <= k < N:
                                if matriz[h][k] == 'x':
                                    contador += 1
                if contador >= 4 or contador <= 1:
                    matriztemp[i][j] = '.'
                else:
                    matriztemp[i][j] = 'x'
                contador = 0

def condicionesmuertas(N, matriz, matriztemp):
    contador = 0
    for i in range(N):
        for j in range(N):
            if matriz[i][j] == '.':
                for h in range(i - 1, i + 2):
                    if 0 <= h < N:
                        for k in range(j - 1, j + 2):
                            if 0 <= k < N:
                                if matriz[h][k] == 'x':
                                    contador += 1
                if contador == 3:
                    matriztemp[i][j] = 'x'
                contador = 0

#main
N = (random.randint(5, 15))
vivas = 0
muertas = 0
matriz = []
texto = ""
crear_matriz(N, matriz)
llenar_matriz(N, matriz)
matriztemp = matriz

while True:
    contar(N, matriztemp)
    imprimir_matriz(N, matriztemp, texto)
    condicionesmuertas(N, matriz, matriztemp)
    condicionesvivas(N, matriz, matriztemp)
    matriz = matriztemp
    time.sleep(2)
    os.system("cls")
