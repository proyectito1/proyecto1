#!/usr/bin/env python
#-*- coding: utf-8 -*-

import os
import random
import time

def crear_matriz(N, matriz):
    for i in range(N):
        matriz.append([0]*N)
    for i in range(N):
        for j in range(N):
            matriz[i][j] = (random.randrange(2))
            if matriz[i][j] == 0:
                matriz[i][j] = 'x'
            if matriz[i][j] == 1:
                matriz[i][j] = '.'

def contar(N, matriz):
    contador_vivas = 0
    contador_muertas = 0
    for i in range(N):
        for j in range(N):
            if matriz[i][j] == 'x':
                contador_vivas += 1
            if matriz[i][j] == '.':
                contador_muertas += 1
    print(" Celulas vivas: ", contador_vivas, '\n', "Celulas muertas: ", contador_muertas, '\n')
    imprimir_matriz(N, matriz, texto)

def imprimir_matriz(N, matriz, texto):
    for i in range(N):
        for j in range(N):
            texto += matriz[i][j] + ' '
        texto += ' \n'
    print(texto)
    condiciones(N, matriz, matriztemp)

def condiciones(N, matriz, matriztemp):
    matriztemp = matriz
    contadorviva = 0
    contadormuerta = 0
    for i in range(N):
        for j in range(N):
            if matriz[i][j] == 'x':
                for h in range(i - 1, i + 2):
                    if 0 <= h < N:
                        for k in range(j - 1, j + 2):
                            if 0 <= k < N:
                                if matriz[h][k] == 'x':
                                    contadorviva += 1
                if contadorviva >= 4:
                    matriztemp[i][j] = '.'
                elif contadorviva <= 1:
                    matriztemp[i][j] = '.'
                else:
                    matriztemp[i][j] = 'x'
                contadorviva = 0

            elif matriz[i][j] == '.':
                for h in range(i - 1, i + 2):
                    if 0 <= h < N:
                        for k in range(j - 1, j + 2):
                            if 0 <= k < N:
                                if matriz[h][k] == 'x':
                                    contadormuerta += 1
                if contadormuerta == 3:
                    matriztemp[i][j] = 'x'
                else:
                    matriztemp[i][j] = '.'
                contador = 0
    matriz=matriztemp
    time.sleep(2)
    os.system("cls")
    contar(N, matriz)

#main
N = (random.randint(5, 15))
vivas = 0
muertas = 0
matriz = []
matriztemp = []
texto = ""
crear_matriz(N, matriz)
contar(N, matriz)

