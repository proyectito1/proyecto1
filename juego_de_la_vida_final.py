#!/usr/bin/env python
# -*- coding: utf-8 -*-
import os
import random
import time

# Función para crear una matriz de tamaño NxN con 1 y 0
def crear_matriz(N, matriz):
    for i in range(N):
        matriz.append([0] * N)
    for i in range(N):
        for j in range(N):
            matriz[i][j] = (random.randrange(2))

# Cambia los 1 por un "-" y los 0 por un "o"
def llenar_matriz(N, matriz):
    for i in range(N):
        for j in range(N):
            if matriz[i][j] == 0:
                matriz[i][j] = 'o'
            if matriz[i][j] == 1:
                matriz[i][j] = '.'

# Recorre la matriz y cuenta las células vivas y muertas
def contar(N, matriz):
    contador_vivas = 0
    contador_muertas = 0
    for i in range(N):
        for j in range(N):
            if matriz[i][j] == 'o':
                contador_vivas += 1
            if matriz[i][j] == '.':
                contador_muertas += 1
    print(" Celulas vivas: ", contador_vivas, '\n', "Celulas muertas: ", contador_muertas, '\n')

# Imprimir matriz
def imprimir_matriz(N, matriz, texto):
    for i in range(N):
        for j in range(N):
            texto += matriz[i][j] + ' '
        texto += ' \n'
    print(texto)

# Si hay una célula viva ve sus ocho vecinos y si el tiene 4 vecinas o mas muere, al igual que si tiene 1 o ninguno
# Si encuentra una célula muerta, también analiza sus ocho vecinos, sie nceuntra 3 vivas, esta vuelve a la vida
def sobreviviencia(N, matriz, matriztemp):
    for i in range(N):
        for j in range(N):
            contador_m = 0
            contador_v = 0
            if matriz[i][j] == 'o':
                for h in range(i - 1, i + 2):
                    for k in range(j - 1, j + 2):
                        if 0 <= k <= N - 1 and k != j and 0 <= h <= N - 1 and h != i:
                            if matriz[h][k] == 'o':
                                contador_v += 1
                if contador_v >= 4 or contador_v <= 1:
                    matriztemp[i][j] = '.'
            elif matriz[i][j] == '.':
                for h in range(i - 1, i + 2):
                    for k in range(j - 1, j + 2):
                        if 0 <= k <= N - 1 and k != j and 0 <= h <= N - 1 and h != i:
                            if matriz[h][k] == 'o':
                                contador_m += 1
                if contador_m == 3:
                    matriztemp[i][j] = 'o'

# Crea la matriz temporal
def crear_temporal(N, matriztemp, matriz):
    for i in range(N):
        matriztemp.append([0] * N)
        for j in range(N):
            matriztemp[i][j] = matriz[i][j]

# Iguala las matrices
def igualar_matrices(N, matriz, matriztemp):
    for i in range(N):
        for j in range(N):
            matriz[i][j] = matriztemp[i][j]


# Main
# N equivale a las dimensiones de la matriz
N = (random.randint(5, 7))
vivas = 0
muertas = 0
matriz = []
matriztemp = []
texto = ""

crear_matriz(N, matriz)
llenar_matriz(N, matriz)
crear_temporal(N, matriztemp, matriz)

# Ciclo para llevar a cabo el juego
while True:
    contar(N, matriz)
    imprimir_matriz(N, matriz, texto)
    sobreviviencia(N, matriz, matriztemp)
    igualar_matrices(N, matriz, matriztemp)
# Tiempo para que se vizualice bien y limpeza de pantalla
    time.sleep(1)
    os.system("clear")