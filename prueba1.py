#!/usr/bin/env python
# -*- coding: utf-8 -*-
import random
import time
import os

def crear_matriz(tamano, matriz):
    for i in range(tamano):
        matriz.append([0]*tamano)
    for i in range(tamano):
        for j in range(tamano):
            matriz[i][j] = (random.randrange(2))
    llenar_matriz(tamano, matriz, juego, contador_vivas , contador_muertas)

def llenar_matriz(tamano, matriz, juego, contador_vivas , contador_muertas):
	for i in range(tamano):
		for j in range(tamano):
			if matriz[i][j] == 0:
				matriz[i][j] = 'o'
				contador_vivas += 1
			if matriz[i][j] == 1:
				matriz[i][j] = '-'
				contador_muertas += 1
			juego += matriz[i][j] + ' '
		juego += '\n'
	print(juego)
	print(" Celulas vivas: ", contador_vivas,'\n',"Celulas muertas: ", contador_muertas, '\n')
	
	time.sleep(1)
	os.system("cls")
	
	crear_matriz(tamano, matriz)


#main
tamano = (random.randint(5, 15))
contador_vivas = 0
contador_muertas = 0
matriz = []
juego = ''
nueva = ''
v_viva = 0

crear_matriz(tamano, matriz)
